FROM golang:1.17.2-alpine

RUN apk update && apk upgrade

ARG REPO_URL
ARG PROJECT_NAME
ARG PROJECT_NAME
ARG BRANCH_NAME
ARG COMMIT

ENV REPO_URL = $CI_REPOSITORY_URL
ENV PROJECT_NAME = $CI_PROJECT_NAME
ENV BRANCH_NAME = $CI_COMMIT_REF_NAME
ENV COMMIT = $CI_COMMIT_SHA

RUN apk add curl
RUN apk add git

WORKDIR /scanner_service

COPY ./code-scanner/go.mod ./code-scanner/go.sum ./code-scanner/
COPY ./code-analyzer ./code-analyzer
RUN cd code-scanner && go mod download

COPY ./code-scanner ./code-scanner

RUN cd code-scanner && go build -o main .

EXPOSE 3002

WORKDIR /scanner_service/code-scanner

CMD ["sh","make.sh","./main" ]