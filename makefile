cert:
	cd cert; sh ./gen.sh; cd ..
.PHONY: gen clean server client test cert 

code-scan:
	cd code-scanner; mkdir tmp; cd tmp; git init;\
    git clone ${CI_PROJECT_URL};\
    cd ${CI_PROJECT_NAME};\
	git checkout "${CI_COMMIT_BRANCH}";\
    git reset --hard ${CI_COMMIT_SHA};\
	curl -v http://localhost:3002/ 


docker-build:
	docker build  --no-cache  -t registry.gitlab.com/psmemee/cd-panisara-test .
docker-push:
	docker push registry.gitlab.com/psmemee/cd-panisara-test:latest