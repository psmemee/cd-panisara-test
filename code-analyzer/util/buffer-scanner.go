package util

import (
	"bufio"

	"github.com/majorperfect/guardrails-test/code-analyzer/service/models"
)

type BufferScannerFunc = func(f *File) (models.Findings, error)

func NewBufferScanner(analyzer LineAnalyzerFunc) BufferScannerFunc {
	return func(f *File) (models.Findings, error) {
		scanner := bufio.NewScanner(f.buffer)
		var lineNum uint32 = 0
		var findings models.Findings
		for scanner.Scan() {
			lineNum++
			finding, found := analyzer(lineNum, scanner.Text(), f.name)
			if found {
				findings.Findings = append(findings.Findings[:], finding)
			}
		}

		if err := scanner.Err(); err != nil {
			return findings, err
		}
		return findings, nil

	}

}
