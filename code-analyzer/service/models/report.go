package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Report struct {
	gorm.Model
	ID        uuid.UUID `gorm:"type:uuid;primary_key;"`
	RepoId    uint32    `gorm:"type:int8;not null;"`
	Commit    string    `gorm:"type:varchar(200);not null;"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (r *Report) TableName() string {
	return "report_reports"
}

func (r *Report) FindOrCreate(db *gorm.DB) error {
	var exist *Report
	err := db.Where("commit = ? and repo_id = ?", r.Commit, r.RepoId).
		Take(&exist).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		r.ID = uuid.New()
		return db.Create(&r).Error
	}
	*r = *exist
	return err
}
