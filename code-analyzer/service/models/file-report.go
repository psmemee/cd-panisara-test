package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type JSONB map[string]interface{}

type FileReport struct {
	gorm.Model
	ReportId   uuid.UUID `gorm:"type:uuid;not null;"`
	Report     Report    `gorm:"foreignKey:report_id;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Path       string    `gorm:"type:varchar(100);not null;"`
	Findings   JSONB     `gorm:"type:jsonb" json:"findings"`
	QueuedAt   time.Time
	ScanningAt time.Time
	FinishAt   time.Time
}

func (fr *FileReport) TableName() string {
	return "report_file_reports"
}

func (fr *FileReport) Create(db *gorm.DB) error {
	return db.Model(&FileReport{}).Create(&fr).Error
}

func (fr *FileReport) Save(db *gorm.DB) error {
	return db.Save(&fr).Error
}

func (fr *FileReport) StampFinished(db *gorm.DB) error {
	fr.FinishAt = time.Now()
	return db.Save(&fr).Error
}

func (fr *FileReport) StampScanning(db *gorm.DB) error {
	fr.ScanningAt = time.Now()
	return db.Save(&fr).Error
}
