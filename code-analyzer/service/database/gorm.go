package database

import (
	"fmt"

	"github.com/majorperfect/guardrails-test/code-analyzer/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type ConnectDBFunc = func() (*gorm.DB, error)

func NewConnectDB(cfg *config.Config) ConnectDBFunc {
	return func() (*gorm.DB, error) {
		dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
			cfg.DBHost, cfg.DBUser, cfg.DBPassword, cfg.DBName, cfg.DBPort)
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err != nil {
			return &gorm.DB{}, err
		}

		return db.Debug(), nil
	}
}
