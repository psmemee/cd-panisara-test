package service

import (
	"context"
	"errors"

	pb "github.com/majorperfect/guardrails-test/code-analyzer/proto/code-analyzer"
	"google.golang.org/grpc"
)

func MakeStreamMock() *StreamMock {
	return &StreamMock{
		ctx:            context.Background(),
		RecvToServer:   make(chan *pb.UploadRequest, 10),
		SentFromServer: make(chan *pb.UploadResponse, 10),
	}
}

type StreamMock struct {
	grpc.ServerStream
	ctx            context.Context
	RecvToServer   chan *pb.UploadRequest
	SentFromServer chan *pb.UploadResponse
}

func (m *StreamMock) Context() context.Context {
	return m.ctx
}
func (m *StreamMock) Send(resp *pb.UploadResponse) error {
	m.SentFromServer <- resp
	return nil
}
func (m *StreamMock) Recv() (*pb.UploadRequest, error) {
	req, more := <-m.RecvToServer
	if !more {
		return nil, errors.New("empty")
	}
	return req, nil
}
func (m *StreamMock) SendFromClient(req *pb.UploadRequest) error {
	m.RecvToServer <- req
	return nil
}
func (m *StreamMock) RecvToClient() (*pb.UploadResponse, error) {
	response, more := <-m.SentFromServer
	if !more {
		return nil, errors.New("empty")
	}
	return response, nil
}
