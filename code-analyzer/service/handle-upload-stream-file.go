package service

import (
	"encoding/json"
	"io"
	"log"
	"time"

	"github.com/majorperfect/guardrails-test/code-analyzer/service/models"
	"gorm.io/gorm"

	pb "github.com/majorperfect/guardrails-test/code-analyzer/proto/code-analyzer"
	"github.com/majorperfect/guardrails-test/code-analyzer/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// read from buffer and scan line by line
type HandleUploadStreamFileFunc = func(stream pb.CodeAnalyzerService_AnalyzeUploaderServer, db *gorm.DB) error

func NewHandleUploadStreamFile(scanBuffer util.BufferScannerFunc) HandleUploadStreamFileFunc {
	return func(stream pb.CodeAnalyzerService_AnalyzeUploaderServer, db *gorm.DB) error {
		firstStream := true
		var newFile *util.File
		//create new report
		var report *models.Report
		var fileReport *models.FileReport
		for {
			req, err := stream.Recv()
			if firstStream {
				// do creating a new report if this is the first file of scanning
				// if commit already exist so use the same record
				newFile = util.NewFile(req.Name)
				report = &models.Report{
					RepoId: req.RepoId,
					Commit: req.CommitId,
				}
				if err := report.FindOrCreate(db); err != nil {
					log.Print(err)
					return status.Error(codes.Internal, err.Error())
				}
				queuedAt, err := time.Parse(time.RFC3339, req.QueueAt)
				if err != nil {
					return status.Error(codes.Internal, err.Error())
				}
				fileReport = &models.FileReport{
					ReportId:   report.ID,
					Path:       req.Name,
					QueuedAt:   queuedAt,
					ScanningAt: time.Now(),
				}
				if err := fileReport.Create(db); err != nil {
					return status.Error(codes.Internal, err.Error())
				}
				firstStream = false
			}
			if err == io.EOF {
				// improve by saving file to storage
				// can do send a worker to analyze a file read from kafka or else
				// for a demo we save in local dir and read it again
				go func() {
					findings, err := scanBuffer(newFile)
					if err != nil {
						log.Print(err)
					}
					if err := fileReport.StampFinished(db); err != nil {
						log.Print(err)
					}

					var jsonB map[string]interface{}
					data, _ := json.Marshal(findings)
					json.Unmarshal(data, &jsonB)
					fileReport.Findings = jsonB

					if err := fileReport.Save(db); err != nil {
						log.Print(err)
					}
				}()
				return stream.SendAndClose(&pb.UploadResponse{Name: newFile.GetName()})
			} else {
				newFile.Write(req.Chunk)
			}
			if err != nil {
				return status.Error(codes.Internal, err.Error())
			}

		}
	}
}
