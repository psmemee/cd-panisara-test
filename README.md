![image info](./img/Untitled Diagram.drawio.png)

this project is a poc of scanning service
there're 2 main services running as a grpc service. code-scanner only do a scanning job and stream to code analyzer service which communicate with TLS authentication. 

**code-scanner**
    scan all files and stream to server for analyzing

**code-analyzer**
    work as an analyzer and screening all files that recieved from code-scanner. The result of scanning will store in database.
**(further) gateway**
    CRUD for user to monitoring their result and others.

