package upload_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/majorperfect/guardrails-test/code-scanner/internal/upload"

	"github.com/majorperfect/guardrails-test/code-scanner/internal"
	"github.com/stretchr/testify/assert"
)

func MockScanner(err error) internal.ScannerFunc {
	return func(ctx context.Context, path string) error {
		return err
	}
}

func TestNewPostUploadHandler(t *testing.T) {
	testCases := []struct {
		testName        string
		mockFileScanner internal.ScannerFunc
		statusCode      int
	}{
		{
			testName:        "should response internal server error",
			mockFileScanner: MockScanner(errors.New("error occured")),
			statusCode:      http.StatusInternalServerError,
		},
		{
			testName:        "should response 200 as successful",
			mockFileScanner: MockScanner(nil),
			statusCode:      http.StatusOK,
		},
	}

	for _, test := range testCases {
		t.Run(test.testName, func(t *testing.T) {
			expected := test.statusCode
			req, _ := http.NewRequest(http.MethodPost, "/", nil)

			resp := httptest.NewRecorder()
			upload.NewPostUploadHandler(test.mockFileScanner)(resp, req)
			actual := resp.Code
			if expected != actual {
				t.Errorf("case: %s, act: %d should be equal %d", test.testName, actual, expected)
			}
			assert.Equal(t, resp.Result().StatusCode, expected)
		})
	}

}

func TestUploadHandler(t *testing.T) {
	testCases := []struct {
		testName        string
		mockFileScanner internal.ScannerFunc
		expected        error
	}{
		{
			testName:        "should return error",
			mockFileScanner: MockScanner(errors.New("error occured")),
			expected:        errors.New("error occured"),
		},
		{
			testName:        "should return nil",
			mockFileScanner: MockScanner(nil),
			expected:        nil,
		},
	}

	for _, test := range testCases {
		t.Run(test.testName, func(t *testing.T) {
			err := upload.UploadHandler(test.mockFileScanner)

			assert.Equal(t, test.expected, err)
		})
	}

}
