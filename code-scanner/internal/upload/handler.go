package upload

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/majorperfect/guardrails-test/code-scanner/internal"
)

func NewPostUploadHandler(fileScanner internal.ScannerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// path := os.Getenv("CI_PROJECT_DIR")
		path := "./tmp"
		if err := fileScanner(r.Context(), path); err != nil {
			log.Printf("fileScanner error: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			if err := json.NewEncoder(w).Encode(map[string]string{"error": "fileScanner"}); err != nil {
				log.Printf("json encode error: %s", err)
			}
			return
		}
	}
}

func UploadHandler(fileScanner internal.ScannerFunc) error {
	// path := os.Getenv("CI_PROJECT_DIR")
	path := "./tmp"

	if err := fileScanner(context.Background(), path); err != nil {
		log.Printf("fileScanner error: %s", err)
		return err
	}
	return nil
}
